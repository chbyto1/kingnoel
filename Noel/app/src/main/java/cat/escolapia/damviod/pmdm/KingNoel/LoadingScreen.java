package cat.escolapia.damviod.pmdm.KingNoel;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by xavier.belda on 11/01/2017.
 */
public class LoadingScreen extends Screen {
    public LoadingScreen   (Game game) {super(game);}

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.jpg", Graphics.PixmapFormat.RGB565);
        Assets.Logo = g.newPixmap("Logo.png", Graphics.PixmapFormat.RGB565);
        Assets.MainMenu = g.newPixmap("MainMenu.png", Graphics.PixmapFormat.ARGB4444);
        Assets.Noel = g.newPixmap("Noel.png", Graphics.PixmapFormat.ARGB4444);
        Assets.King = g.newPixmap("King.jpg", Graphics.PixmapFormat.ARGB4444);
        Assets.buttons = g.newPixmap("button.png", Graphics.PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pausemenu.png", Graphics.PixmapFormat.ARGB4444);

        game.setScreen(new MainMenu(game));
    }

    @Override
    public void render(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }


}
