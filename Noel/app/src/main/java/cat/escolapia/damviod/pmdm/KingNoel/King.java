package cat.escolapia.damviod.pmdm.KingNoel;

/**
 * Created by xavier.belda on 11/01/2017.
 */
public class King {
    public static final int UP = 0;
    public static final int LEFT = 1;
    public static final int DOWN = 2;
    public static final int RIGHT = 3;
    public int x, y;
    public int direction;
    public King() {
    }

    public void setDirection(int direction)
    {
      this.direction = direction;
        if(direction == UP)
        {
            y = y - 1;}
        if(direction == LEFT)
        { x =x - 1;}
        if(direction == DOWN)
        { y = y + 1;}
        if(direction == RIGHT){ x = x + 1;}
    }
}
