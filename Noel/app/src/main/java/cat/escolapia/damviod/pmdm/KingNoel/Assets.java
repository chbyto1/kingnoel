package cat.escolapia.damviod.pmdm.KingNoel;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;
/**
 * Created by xavier.belda on 11/01/2017.
 */
public class Assets {
    public static Pixmap background;
    public static Pixmap Logo;
    public static Pixmap MainMenu;
    public static Pixmap Noel;
    public static Pixmap King;
    public static Pixmap buttons;
    public static Pixmap pause;
}
