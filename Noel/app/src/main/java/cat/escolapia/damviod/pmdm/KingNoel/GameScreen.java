package cat.escolapia.damviod.pmdm.KingNoel;

import android.graphics.Color;

import java.util.List;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by xavier.belda on 11/01/2017.
 */
public class GameScreen extends Screen{
    enum GameState{Ready,Running,Paused,GameOver}

    GameState state = GameState.Ready;
    World world;

    public GameScreen(Game game)
    {
        super(game);
        world = new World();
    }
    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(state == GameState.Ready)
            updateReady(touchEvents);
        if(state == GameState.Running)
            updateRunning(touchEvents,deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);
    }

    private void updateReady(List<TouchEvent> touchEvents)
    {
        if(touchEvents.size() > 0 )
            state = GameState.Running;
    }
    private void updateRunning (List<TouchEvent> touchEvents, float deltaTime)
    {
        float desx=0;
        float desy=0;
        float x1,x2,y1,y2;
        int len = touchEvents.size();
        for(int i = 0; i < len;i++)
        {
            TouchEvent event = touchEvents.get(i);
            //switch (event.type) {
                //case TouchEvent.TOUCH_DOWN:
            if(event.type == TouchEvent.TOUCH_DOWN)
                    desy = event.x;
                    desx = event.y;
                    //break;
                //case TouchEvent.TOUCH_UP: {
            if(event.type == TouchEvent.TOUCH_UP)
            {
                    x2 = event.x;
                    y2 = event.y;
                    x1 = x2 - desx;
                    y1 = y2 - desy;
                if(event.x < 64 && event.y < 64) {

                    state = GameState.Paused;
                    return;
                }

                    if (x1 > 0 && Math.abs(y1) > Math.abs(x1)) {
                        world.king.setDirection(King.RIGHT);
                    }
                    else if (x1 < 0 && Math.abs(y1) > Math.abs(x1)) {
                        world.king.setDirection(King.LEFT);
                    }
                    else if (y1 > 0 && Math.abs(y1) < Math.abs(x1)) {
                        world.king.setDirection(King.UP);
                    }
                    else if (y1 < 0 && Math.abs(y1) < Math.abs(x1)) {
                        world.king.setDirection(King.DOWN);
                    }
                    //break;
                }

        }
        world.update(deltaTime);
        ////////////GAME OVER ///////////////
    }
    private void updatePaused(List<TouchEvent> touchEvents)
    {
        int len = touchEvents.size();
        for (int i = 0; i< len;i++)
        {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP)
            {
                if(event.x > 100 && event.y < 148)
                {
                    state = GameState.Running;
                    return;
                }
                if(event.y > 148 && event.y < 196)
                {
                    game.setScreen(new MainMenu(game));
                    return;
                }
            }

        }
    }
    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                        event.y >= 200 && event.y <= 264) {
                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }
    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background, 0, 0);
        drawWorld(world);

        if(state == GameState.Ready)
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        /*if(state == GameState.GameOver)
            drawGameOverUI();

        drawText(g, world.getScore(), g.getWidth() / 2 - score.length()*20 / 2, g.getHeight() - 42);
        */
    }

    private void drawWorld(World world)
    {
        Graphics g = game.getGraphics();
        King king = world.king;
        Pixmap noelPixmap = null;
        Pixmap kingPixmap = null;
        ////////// Dibujar los Noeles/////////
        int x;
        int y;
        noelPixmap = Assets.Noel;
        for(int i = 1; i<14;i++)
        {
            g.drawPixmap(noelPixmap,0,i*32);
        }
        kingPixmap = Assets.King;
        x = king.x *32;
        y = king.y * 32;
        g.drawPixmap(kingPixmap,x,y);
    }
    private void drawReadyUI()
    {   /*
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.ready,47,100);
        g.drawLine(0,416,480,416, Color.WHITE);
        */
    }

    private void drawRunningUI()
    {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.buttons, 0, 0, 64, 128, 64, 64);
        g.drawPixmap(Assets.buttons,0,0);

    }
    private void drawPausedUI()
    {  Graphics g = game.getGraphics();

        g.drawPixmap(Assets.pause, 80, 100);}
    public void drawText(Graphics g,String line, int x, int y)
    {}
    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
/*
        if(world.gameOver) {
            Settings.save(game.getFileIO());
        }*/
    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
