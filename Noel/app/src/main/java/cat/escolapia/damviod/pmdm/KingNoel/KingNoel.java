package cat.escolapia.damviod.pmdm.KingNoel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.impl.AndroidGame;
import cat.escolapia.damviod.pmdm.noel.R;

public class KingNoel extends AndroidGame {
    public Screen getStartScreen() {return new LoadingScreen(this);
    }
}
