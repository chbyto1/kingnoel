package cat.escolapia.damviod.pmdm.KingNoel;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by xavier.belda on 11/01/2017.
 */
public class World {

    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.02f;

    public Noel noel;
    public King king;
    public Noel[] noels = new Noel[13];
    float tickTime = 0;
    float tick = TICK_INITIAL;

    //////////////INSTANCIAR OBJETOS
    public World()
    {
        noel = new Noel(0,0);
        king = new King();
        king.x = 4;
        king.y = 14;

        for(int i=0; i<13;i++)
        {
            noels[i] = new Noel(0,i);
        }
    }

    public void update(float deltaTime)
    {
        tickTime +=deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            noel.Move();
            //Colisiones con los diamantes aumenteamos la serpiente

        }
    }

}
